<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$module_id = "mlife.smsservices";
$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);
$zr = "";
$phone = '';
if (! ($MODULE_RIGHT >= "R"))
	$APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
?>
<?
$APPLICATION->SetTitle(Loc::getMessage("MLIFESS_SENDFORM_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
$APPLICATION->SetAdditionalCSS("/bitrix/modules/mlife.smsservices/assets/css/style.css");

\Bitrix\Main\Loader::includeModule($module_id);
$smsServices = new \Mlife\Smsservices\Sender();

//TODO ����� �������� � ��������� �����, ����������� �������� �����������
//$senderOptions = $smsServices->getAllSender();
if ($_SERVER["REQUEST_METHOD"] == "POST" && $MODULE_RIGHT == "W" && strlen($_REQUEST["Send"]) > 0)
{
	$message = strip_tags($_REQUEST['message']);

	if(!$_REQUEST['phone'] or !$_REQUEST['message']) {
		\ShowNote(Loc::getMessage("MLIFESS_SENDFORM_ERR_REQ"),'mlifeerror');
	}
	else{
		
		$phoneCheck = $smsServices->checkPhoneNumber($_REQUEST['phone']);
		$phone = $phoneCheck['phone'];
		
		if($phoneCheck['check']) {
			$resp = $smsServices->sendSms($phone, $message, MakeTimeStamp($datesend, "DD.MM.YYYY HH:MI:SS"),false,Loc::getMessage("MLIFESS_SENDFORM_PRIM"));
			if(!$resp->error){
				\ShowNote(Loc::getMessage("MLIFESS_SENDFORM_NOTICE_SEND"),'mlifenoticeok');
				$phone = false;
				$message = false;
			}else{
				\ShowNote(Loc::getMessage("MLIFESS_SENDFORM_ERR_".$resp->error),'mlifeerror');
			}
		}
		else{
			\ShowNote(Loc::getMessage("MLIFESS_SENDFORM_ERR_PHONE").', '.$phone,'mlifeerror');
		}
		
	}
	
}


$aTabs = array(
	array("DIV" => "edit3", "TAB" => Loc::getMessage("MLIFESS_SENDFORM_TAB"), "ICON" => "vote_settings", "TITLE" => Loc::getMessage("MLIFESS_SENDFORM_TAB")),
);
$tabControl = new \CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();
?>
<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?lang=<?=LANGUAGE_ID?>" id="FORMACTION">
<?
$tabControl->BeginNextTab();
?>
	<tr>
		<td><?=Loc::getMessage("MLIFESS_SENDFORM_PHONE")?>*:</td>
		<td>
			<input type="text" size="28" maxlength="255" value="<?=$phone?>" name="phone">
		</td>
		</td>
	</tr>
	<tr>
		<td><?=Loc::getMessage("MLIFESS_SENDFORM_MESS")?>*:</td>
		<td>
			<textarea cols="27" rows="5" name="message"><?=$message?></textarea>
		</td>
	</tr>

	<tr>
		<td><?=Loc::getMessage("MLIFESS_SENDFORM_DATE")?>:</td>
		<td>
			<?echo \CalendarDate("datesend", $_REQUEST['datesend'], "datesend", "25", "class=\"date\"")?>
		</td>
	</tr>
	<?
$tabControl->Buttons();
?>
	<input <?if ($MODULE_RIGHT<"W") echo "disabled" ?> type="submit" class="adm-btn-green" name="Sendform" value="<?=Loc::getMessage("MLIFESS_SENDFORM_SEND")?>" />
	<input type="hidden" name="Send" value="Y" />
<?$tabControl->End();
?>
</form>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>