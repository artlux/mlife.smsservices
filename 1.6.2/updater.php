<?
if(IsModuleInstalled("mlife.smsservices")) {
	global $DB;
	$sql = "
	CREATE TABLE IF NOT EXISTS `mlife_smsservices_list` (
	  `ID` int(18) NOT NULL AUTO_INCREMENT,
	  `PROVIDER` varchar(50) DEFAULT NULL,
	  `SMSID` varchar(100) DEFAULT NULL,
	  `SENDER` varchar(50) DEFAULT NULL,
	  `PHONE` varchar(20) DEFAULT NULL,
	  `TIME` int(11) NOT NULL,
	  `TIME_ST` int(11) NOT NULL,
	  `MEWSS` varchar(655) NOT NULL,
	  `PRIM` varchar(655) DEFAULT NULL,
	  `STATUS` int(2) NOT NULL DEFAULT '0',
	  PRIMARY KEY (`id`)
	) AUTO_INCREMENT=1 ;
	";
	if(strtolower($DB->type)=="mysql") $res = $DB->Query($sql);
	
	$sql = 'DROP TABLE IF EXISTS `b_mlife_smsservices_list`';
	$res = $DB->Query($sql);
	
	UnRegisterModuleDependences("sale", "OnSaleStatusOrder", "mlife.smsservices", "CmlifeCmsServicesHandlers", "OnSaleStatusOrderHandler");
	UnRegisterModuleDependences("sale", "OnSaleCancelOrder", "mlife.smsservices", "CmlifeCmsServicesHandlers", "OnSaleCancelOrderHandler");
	UnRegisterModuleDependences("sale", "OnSaleComponentOrderOneStepComplete", "mlife.smsservices", "CmlifeCmsServicesHandlers", "OnSaleComponentOrderOneStepCompleteHandler");
	UnRegisterModuleDependences("sale", "OnSaleComponentOrderComplete", "mlife.smsservices", "CmlifeCmsServicesHandlers", "OnSaleComponentOrderOneStepCompleteHandler");
	UnRegisterModuleDependences("sale", "OnSaleCancelOrder", "mlife.smsservices", "CmlifeCmsServicesHandlers", "OnSaleCancelOrderHandler");
	UnRegisterModuleDependences("sale", "OnSaleDeliveryOrder", "mlife.smsservices", "CmlifeCmsServicesHandlers", "OnSaleDeliveryOrderHandler");
	
	RegisterModuleDependences("sale", "OnSaleStatusOrder", "mlife.smsservices", "\\Mlife\\Smsservices\\Handlers", "OnSaleStatusOrderHandler");
	RegisterModuleDependences("sale", "OnSaleCancelOrder", "mlife.smsservices", "\\Mlife\\Smsservices\\Handlers", "OnSaleCancelOrderHandler");
	RegisterModuleDependences("sale", "OnSaleComponentOrderOneStepComplete", "mlife.smsservices", "\\Mlife\\Smsservices\\Handlers", "OnSaleComponentOrderOneStepCompleteHandler");
	RegisterModuleDependences("sale", "OnSaleComponentOrderComplete", "mlife.smsservices", "\\Mlife\\Smsservices\\Handlers", "OnSaleComponentOrderOneStepCompleteHandler");
	RegisterModuleDependences("sale", "OnSaleCancelOrder", "mlife.smsservices", "\\Mlife\\Smsservices\\Handlers", "OnSaleCancelOrderHandler");
	RegisterModuleDependences("sale", "OnSaleDeliveryOrder", "mlife.smsservices", "\\Mlife\\Smsservices\\Handlers", "OnSaleDeliveryOrderHandler");
	
	CAgent::RemoveAgent("CMlifeSmsServicesAgentStatusSms();", "mlife.smsservices");
	CAgent::RemoveAgent("CMlifeSmsServicesAgentTurnSms();", "mlife.smsservices");
	
	CAgent::AddAgent(
	"\\Mlife\\Smsservices\\Agent::statusSms();",
	"mlife.smsservices",
	"N",
	600);
	CAgent::AddAgent(
	"\\Mlife\\Smsservices\\Agent::turnSms();",
	"mlife.smsservices",
	"N",
	300);
	
}
?>
