<?
//старый метод для отправки смс
class CMlifeSmsServices {
	
	function __construct() {
		$this->transport = new \Mlife\Smsservices\Sender();
	}
	
	public function checkPhoneNumber ($phone,$all=true) {
		return $this->transport->checkPhoneNumber($phone,$all);
	}
	
	public function sendSms($phones, $mess, $time=0, $sender=false, $prim='', $addHistory=true, $update=false, $error=false) {
		return $this->transport->sendSms($phones, $mess, $time, $sender, $prim, $addHistory, $update, $error);
	}
	
}
?>