<?
if(IsModuleInstalled("mlife.smsservices")) {
	if(IsModuleInstalled("sale")) {
		RegisterModuleDependences("sale", "OnSaleStatusOrder", "mlife.smsservices", "CmlifeCmsServicesHandlers", "OnSaleStatusOrderHandler");
		RegisterModuleDependences("sale", "OnSaleCancelOrder", "mlife.smsservices", "CmlifeCmsServicesHandlers", "OnSaleCancelOrderHandler");
		RegisterModuleDependences("sale", "OnSaleComponentOrderOneStepComplete", "mlife.smsservices", "CmlifeCmsServicesHandlers", "OnSaleComponentOrderOneStepCompleteHandler");
		RegisterModuleDependences("sale", "OnSaleComponentOrderComplete", "mlife.smsservices", "CmlifeCmsServicesHandlers", "OnSaleComponentOrderOneStepCompleteHandler");
		RegisterModuleDependences("sale", "OnSaleCancelOrder", "mlife.smsservices", "CmlifeCmsServicesHandlers", "OnSaleCancelOrderHandler");
		RegisterModuleDependences("sale", "OnSaleDeliveryOrder", "mlife.smsservices", "CmlifeCmsServicesHandlers", "OnSaleDeliveryOrderHandler");
	}
}
?>
