<?
IncludeModuleLangFile(__FILE__);

Class mlife_smsservices extends CModule
{
        var $MODULE_ID = "mlife.smsservices";
        var $MODULE_VERSION;
        var $MODULE_VERSION_DATE;
        var $MODULE_NAME;
        var $MODULE_DESCRIPTION;

        function mlife_smsservices() {
				$path = str_replace("\\", "/", __FILE__);
				$path = substr($path, 0, strlen($path) - strlen("/index.php"));
				include($path."/version.php");
				
				$this->MODULE_VERSION = $arModuleVersion["VERSION"];
				$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
				$this->PARTNER_NAME = GetMessage("MLIFESS_PARTNER_NAME");
				$this->PARTNER_URI = GetMessage("MLIFESS_PARTNER_URI");
				$this->MODULE_NAME = GetMessage("MLIFESS_MODULE_NAME");
				$this->MODULE_DESCRIPTION = GetMessage("MLIFESS_MODULE_DESC");
				
			return true;
        }

        function DoInstall() {
			
			CopyDirFiles(
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlife.smsservices/install/admin",
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
			
			RegisterModule($this->MODULE_ID);
			$this->createTable();
			$this->createAgents();
			
			RegisterModuleDependences("sale", "OnSaleStatusOrder", $this->MODULE_ID, "CmlifeCmsServicesHandlers", "OnSaleStatusOrderHandler");
			RegisterModuleDependences("sale", "OnSaleCancelOrder", $this->MODULE_ID, "CmlifeCmsServicesHandlers", "OnSaleCancelOrderHandler");
			RegisterModuleDependences("sale", "OnSaleComponentOrderOneStepComplete", $this->MODULE_ID, "CmlifeCmsServicesHandlers", "OnSaleComponentOrderOneStepCompleteHandler");
			RegisterModuleDependences("sale", "OnSaleComponentOrderComplete", $this->MODULE_ID, "CmlifeCmsServicesHandlers", "OnSaleComponentOrderOneStepCompleteHandler");
			RegisterModuleDependences("sale", "OnSaleCancelOrder", $this->MODULE_ID, "CmlifeCmsServicesHandlers", "OnSaleCancelOrderHandler");
			RegisterModuleDependences("sale", "OnSaleDeliveryOrder", $this->MODULE_ID, "CmlifeCmsServicesHandlers", "OnSaleDeliveryOrderHandler");
			
			//�������� �� ��������� ����������
			LocalRedirect('/bitrix/admin/settings.php?lang=ru&mid=mlife.smsservices&mid_menu=1');
        }

        function DoUninstall() {
			//�������� ������ ���������� ����� ����� ������
			DeleteDirFiles(
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlife.smsservices/install/admin",
			$_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
			
			$this->deleteTable();
			$this->deleteAgents();
			
			UnRegisterModuleDependences("sale", "OnSaleStatusOrder", $this->MODULE_ID, "CmlifeCmsServicesHandlers", "OnSaleStatusOrderHandler");
			UnRegisterModuleDependences("sale", "OnSaleCancelOrder", $this->MODULE_ID, "CmlifeCmsServicesHandlers", "OnSaleCancelOrderHandler");
			UnRegisterModuleDependences("sale", "OnSaleComponentOrderOneStepComplete", $this->MODULE_ID, "CmlifeCmsServicesHandlers", "OnSaleComponentOrderOneStepCompleteHandler");
			UnRegisterModuleDependences("sale", "OnSaleComponentOrderComplete", $this->MODULE_ID, "CmlifeCmsServicesHandlers", "OnSaleComponentOrderOneStepCompleteHandler");
			UnRegisterModuleDependences("sale", "OnSaleCancelOrder", $this->MODULE_ID, "CmlifeCmsServicesHandlers", "OnSaleCancelOrderHandler");
			UnRegisterModuleDependences("sale", "OnSaleDeliveryOrder", $this->MODULE_ID, "CmlifeCmsServicesHandlers", "OnSaleDeliveryOrderHandler");
			
			UnRegisterModule($this->MODULE_ID);
        }
	
	function createTable() {
		global $DB;
		$sql = "
		CREATE TABLE IF NOT EXISTS `b_mlife_smsservices_list` (
		  `id` int(18) NOT NULL AUTO_INCREMENT,
		  `provider` varchar(50) DEFAULT NULL,
		  `smsid` varchar(100) DEFAULT NULL,
		  `sender` varchar(50) DEFAULT NULL,
		  `phone` varchar(20) DEFAULT NULL,
		  `time` int(11) NOT NULL,
		  `time_st` int(11) NOT NULL,
		  `mess` text NOT NULL,
		  `prim` text NOT NULL,
		  `status` int(2) NOT NULL DEFAULT '0',
		  PRIMARY KEY (`id`)
		) AUTO_INCREMENT=1 ;
		";
		if(strtolower($DB->type)=="mysql") $res = $DB->Query($sql);
	}
	
	function deleteTable () {
		global $DB;
		$sql = 'DROP TABLE IF EXISTS `b_mlife_smsservices_list`';
		$res = $DB->Query($sql);
	}
	
	function createAgents() {
		CAgent::AddAgent(
		"CMlifeSmsServicesAgentStatusSms();",
		"mlife.smsservices",
		"N",
		600);
		CAgent::AddAgent(
		"CMlifeSmsServicesAgentTurnSms();",
		"mlife.smsservices",
		"N",
		300);
	}
	
	function deleteAgents() {
		CAgent::RemoveAgent("CMlifeSmsServicesAgentStatusSms();", "mlife.smsservices");
		CAgent::RemoveAgent("CMlifeSmsServicesAgentTurnSms();", "mlife.smsservices");
	}
}

?>

