<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
IncludeModuleLangFile(__FILE__);
$module_id = "mlife.smsservices";
$MODULE_RIGHT = $APPLICATION->GetGroupRight($module_id);
$zr = "";
if (! ($MODULE_RIGHT >= "R"))
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$APPLICATION->SetTitle(GetMessage("MLIFESS_OPT_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

CModule::IncludeModule('mlife.smsservices');
$smsServices = new CMlifeSmsServices();

$bollModuleSale = CModule::IncludeModule("sale");

	if($bollModuleSale) {
		
		$arMakros = array();
		$arOrderProps = array();
		$arStatus = array();
		$arPerson = array();
		
		//свойства
		$db_props = CSaleOrderProps::GetList(array("SORT" => "ASC"),array("TYPE" => array('TEXT','TEXTAREA')));
		while($arProperty = $db_props->Fetch()) {
			$arOrderProps[$arProperty["PERSON_TYPE_ID"]][$arProperty['CODE']] = $arProperty['NAME'];
			$arMakros[$arProperty["PERSON_TYPE_ID"]]['PROPERTY_'.$arProperty['CODE']] = $arProperty['NAME'];
			$arMakros[$arProperty["PERSON_TYPE_ID"]]['USER_PHONE'] = GetMessage("MLIFESS_IM_MACROS1");
			$arMakros[$arProperty["PERSON_TYPE_ID"]]['ORDER_NUM'] = GetMessage("MLIFESS_IM_MACROS2");
			$arMakros[$arProperty["PERSON_TYPE_ID"]]['ORDER_PRICE'] = GetMessage("MLIFESS_IM_MACROS3");
			$arMakros[$arProperty["PERSON_TYPE_ID"]]['DELIVERY_PRICE'] = GetMessage("MLIFESS_IM_MACROS4");
			$arMakros[$arProperty["PERSON_TYPE_ID"]]['STATUS_NAME'] = GetMessage("MLIFESS_IM_MACROS5");
			$arMakros[$arProperty["PERSON_TYPE_ID"]]['DELIVERY_NAME'] = GetMessage("MLIFESS_IM_MACROS6");
			$arMakros[$arProperty["PERSON_TYPE_ID"]]['ORDER_SUM'] = GetMessage("MLIFESS_IM_MACROS7");
		}
		
		//статусы
		$obStatus = CSaleStatus::GetList();
		while($ar = $obStatus->Fetch()) {
			$arStatus[$ar['ID']][$ar['LID']] = $ar['NAME'];
		}
		
		//типы плательщиков
		$db_ptype = CSalePersonType::GetList(Array("SORT" => "ASC"), Array());
		$bFirst = True;
		while ($ptype = $db_ptype->Fetch())
		{
			$arPerson[$ptype["ID"]] = $ptype["NAME"];
		}
	
	}

$arSites = array();
$obSite = CSite::GetList($by="sort", $order="desc");
while($arResult = $obSite->Fetch()) {
	$arSites[$arResult['ID']] = $arResult['NAME'];
}
	
if ($_SERVER["REQUEST_METHOD"] == "POST" && $MODULE_RIGHT == "W" && strlen($_REQUEST["Update"]) > 0 && check_bitrix_sessid())
{
COption::SetOptionString("mlife.smsservices", "transport", $_REQUEST["transport"]);
COption::SetOptionString("mlife.smsservices", "login", $_REQUEST["login"]);
COption::SetOptionString("mlife.smsservices", "passw", $_REQUEST["passw"]);
COption::SetOptionString("mlife.smsservices", "sender", $_REQUEST["sender"]);
COption::SetOptionString("mlife.smsservices", "charset", $_REQUEST["charset"]);
COption::SetOptionString("mlife.smsservices", "cacheotp", $_REQUEST["cacheotp"]);
COption::SetOptionString("mlife.smsservices", "listotp", $_REQUEST["listotp"]);
COption::SetOptionString("mlife.smsservices", "cachebalance", $_REQUEST["cachebalance"]);
COption::SetOptionString("mlife.smsservices", "activesale", $_REQUEST["activesale"]);

	foreach($arSites as $siteId=>$siteName){
		foreach($arPerson as $persid=>$persName){
			COption::SetOptionString("mlife.smsservices", "property_phone_".$siteId."_".$persid, $_REQUEST["property_phone_".$siteId."_".$persid]);
			COption::SetOptionString("mlife.smsservices", "admin_phone_".$siteId."_".$persid, $_REQUEST["admin_phone_".$siteId."_".$persid]);
			
			COption::SetOptionString("mlife.smsservices", "mess_status_".$siteId."_cancelY_".$persid, $_REQUEST["mess_status_".$siteId."_cancelY_".$persid]);
			COption::SetOptionString("mlife.smsservices", "mess_status_".$siteId."_cancelN_".$persid, $_REQUEST["mess_status_".$siteId."_cancelN_".$persid]);
			COption::SetOptionString("mlife.smsservices", "mess_status_".$siteId."_deliveryN_".$persid, $_REQUEST["mess_status_".$siteId."_deliveryN_".$persid]);
			COption::SetOptionString("mlife.smsservices", "mess_status_".$siteId."_deliveryY_".$persid, $_REQUEST["mess_status_".$siteId."_deliveryY_".$persid]);
			COption::SetOptionString("mlife.smsservices", "mess_status_".$siteId."_payN_".$persid, $_REQUEST["mess_status_".$siteId."_payN_".$persid]);
			COption::SetOptionString("mlife.smsservices", "mess_status_".$siteId."_payY_".$persid, $_REQUEST["mess_status_".$siteId."_payY_".$persid]);
			COption::SetOptionString("mlife.smsservices", "mess_status_".$siteId."_new_".$persid, $_REQUEST["mess_status_".$siteId."_new_".$persid]);
			COption::SetOptionString("mlife.smsservices", "mess_status_".$siteId."_new2_".$persid, $_REQUEST["mess_status_".$siteId."_new2_".$persid]);
			
			foreach($arStatus as $statusid=>$statusname) {
				
				COption::SetOptionString("mlife.smsservices", "mess_status_".$siteId."_".$statusid."_".$persid, $_REQUEST["mess_status_".$siteId."_".$statusid."_".$persid]);
			
			}
		}
	}

	}

$actSale = COption::GetOptionString("mlife.smsservices", "activesale", "N");
$actSale = ($actSale=="Y") ? true : false;

$opt = $smsServices->getAllSenderOptions();

$aTabs = array();
$aTabs[] = array("DIV" => "edit3", "TAB" => GetMessage("MLIFESS_OPT_TAB1"), "ICON" => "vote_settings", "TITLE" => GetMessage("MLIFESS_OPT_TAB1_T"));
if($bollModuleSale && $actSale){

	foreach($arSites as $siteId=>$siteName){
		$aTabs[] = array("DIV" => "edit".$siteId, "TAB" => "[".$siteId."] ".$siteName, "ICON" => "vote_settings5", "TITLE" => "[".$siteId."] ".$siteName);
	}
	
}
$aTabs[] = array("DIV" => "edit4", "TAB" => GetMessage("MLIFESS_OPT_TAB2"), "ICON" => "vote_settings2", "TITLE" => GetMessage("MLIFESS_OPT_TAB2_T"));

$tabControl = new CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();
?>
<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($module_id)?>&lang=<?=LANGUAGE_ID?>&mid_menu=1" id="FORMACTION">
<?
$tabControl->BeginNextTab();
?>
	<tr>
		<td><?=GetMessage("MLIFESS_OPT_SHLUZ")?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "transport", "smsc.ru");?>
			<select name="transport" id="transport">
			<?
			$smslist = glob($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlife.smsservices/classes/general/transport/*.php");
			foreach ($smslist as $value) {
				$name = str_replace($_SERVER["DOCUMENT_ROOT"].'/bitrix/modules/mlife.smsservices/classes/general/transport/','',$value);
				$selected = '';
				if($val==$name) $selected = ' selected';
				echo '<option value="'.$name.'"'.$selected.'>'.GetMessage("SERVIS_".strtoupper(str_replace('.php','',$name))).'</option>';
			}
			?>
			</select>
		</td>
	</tr>
	<tr>
		<td><?=GetMessage("MLIFESS_OPT_LOGIN")?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "login", "");?>
			<input type="text" size="35" maxlength="255" value="<?=$val?>" name="login"></td>
	</tr>
	<tr>
		<td><?=GetMessage("MLIFESS_OPT_PASSW")?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "passw", "");?>
			<input type="text" size="35" maxlength="255" value="<?=$val?>" name="passw"></td>
	</tr>
	<tr>
		<td><?=GetMessage("MLIFESS_OPT_OTP")?>:</td>
		<td>
		<?
		if(COption::GetOptionString("mlife.smsservices", "listotp")=='Y' && $opt!=''){
		?>
			<select name="sender" id="sender">
			<?=$opt;?>
			</select>
		<?} else {
		$val = COption::GetOptionString("mlife.smsservices", "sender", ".");?>
			<input type="text" size="35" maxlength="255" value="<?=$val?>" name="sender">
		<?}?>
		</td>
	</tr>
	<tr>
		<td><?=GetMessage("MLIFESS_OPT_OTPLIST")?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "listotp", "N");?>
			<input type="checkbox" value="Y" name="listotp" id="listotp" <?if ($val=="Y") echo "checked";?>></td>
	</tr>
	<tr>
		<td><?=GetMessage("MLIFESS_OPT_OTPCACHE")?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "cacheotp", "86400");?>
			<input type="text" size="35" maxlength="255" value="<?=$val?>" name="cacheotp"></td>
	</tr>
	<tr>
		<td><?=GetMessage("MLIFESS_OPT_BALANCECACHE")?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "cachebalance", "3600");?>
			<input type="text" size="35" maxlength="255" value="<?=$val?>" name="cachebalance"></td>
	</tr>
	<tr>
		<td><?=GetMessage("MLIFESS_OPT_CARSET")?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "charset", "windows-1251");?>
			<input type="hidden" name="charset" value="<?if(ToLower(SITE_CHARSET) == "windows-1251") {echo 'windows-1251';}else{echo 'utf-8';}?>"/><?echo $val;?>
			</td>
	</tr>
	<?
	if($bollModuleSale){
	?>
	<tr>
		<td><?=GetMessage("MLIFESS_IM_ON")?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "activesale", "N");?>
			<input type="checkbox" value="Y" name="activesale" id="activesale" <?if ($val=="Y") echo "checked";?>></td>
	</tr>
	<?}?>
<?
if($bollModuleSale && $actSale){
?>
<?
foreach($arSites as $siteId=>$siteName){
	$tabControl->BeginNextTab();
	?>
	<?
	foreach($arPerson as $persid=>$persName){
	
	?>
	<tr>
		<td style="width:50%;"><?=GetMessage("MLIFESS_IM_PROP_PHONE")?> <?=$persName?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "property_phone_".$siteId."_".$persid, "");?>
			<?echo CMlifeSmsServicesHtml::getSelect("property_phone_".$siteId."_".$persid,$arOrderProps[$persid],$val,false,false,true);?>
		</td>
	</tr>
	<tr>
		<td style="width:50%;"><?=GetMessage("MLIFESS_IM_PROP_PHONEADMIN")?>, <?=GetMessage("MLIFESS_IM_PROP_DLYA")?> <?=$persName?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "admin_phone_".$siteId."_".$persid, "");?>
			<input type="text" size="35" maxlength="255" value="<?=$val?>" name="admin_phone_<?=$siteId?>_<?=$persid?>"></td>
		</td>
	</tr>
	<tr>
		<td><?=GetMessage("MLIFESS_IM_PROP_MACROS")?>:</td>
		<td>
		<?foreach($arMakros[$persid] as $macros=>$nacrosName){
			?>
			#<?=$macros?># - <?=$nacrosName?>; 
			<?
		}?>
		</td>
	</tr>
	<?
	
	foreach($arStatus as $statusid=>$statusname) {
	?>
	<tr>
		<td style="width:50%;"><?=GetMessage("MLIFESS_IM_PROP_STATUS")?> <?=$statusname[LANGUAGE_ID]?>, <?=GetMessage("MLIFESS_IM_PROP_DLYA")?> <?=$persName?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "mess_status_".$siteId."_".$statusid."_".$persid, "");?>
			<textarea style="width:90%;" name="mess_status_<?=$siteId?>_<?=$statusid?>_<?=$persid?>" id="mess_status_<?=$siteId?>_<?=$statusid?>_<?=$persid?>"><?=$val?></textarea>
		</td>
	</tr>
	<?
	}
	?>
	<tr>
		<td style="width:50%;"><?=GetMessage("MLIFESS_IM_PROP_STATUS_OTMY")?>, <?=GetMessage("MLIFESS_IM_PROP_DLYA")?> <?=$persName?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "mess_status_".$siteId."_cancelY_".$persid, "");?>
			<textarea style="width:90%;" name="mess_status_<?=$siteId?>_cancelY_<?=$persid?>" id="mess_status_<?=$siteId?>_cancelY_<?=$persid?>"><?=$val?></textarea>
		</td>
	</tr>
	<tr>
		<td style="width:50%;"><?=GetMessage("MLIFESS_IM_PROP_STATUS_OTMN")?>, <?=GetMessage("MLIFESS_IM_PROP_DLYA")?> <?=$persName?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "mess_status_".$siteId."_cancelN_".$persid, "");?>
			<textarea style="width:90%;" name="mess_status_<?=$siteId?>_cancelN_<?=$persid?>" id="mess_status_<?=$siteId?>_cancelN_<?=$persid?>"><?=$val?></textarea>
		</td>
	</tr>
	<tr>
		<td style="width:50%;"><?=GetMessage("MLIFESS_IM_PROP_STATUS_DOSTY")?>, <?=GetMessage("MLIFESS_IM_PROP_DLYA")?> <?=$persName?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "mess_status_".$siteId."_deliveryY_".$persid, "");?>
			<textarea style="width:90%;" name="mess_status_<?=$siteId?>_deliveryY_<?=$persid?>" id="mess_status_<?=$siteId?>_deliveryY_<?=$persid?>"><?=$val?></textarea>
		</td>
	</tr>
	<tr>
		<td style="width:50%;"><?=GetMessage("MLIFESS_IM_PROP_STATUS_DOSTN")?>, <?=GetMessage("MLIFESS_IM_PROP_DLYA")?> <?=$persName?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "mess_status_".$siteId."_deliveryN_".$persid, "");?>
			<textarea style="width:90%;" name="mess_status_<?=$siteId?>_deliveryN_<?=$persid?>" id="mess_status_<?=$siteId?>_deliveryN_<?=$persid?>"><?=$val?></textarea>
		</td>
	</tr>
	<tr>
		<td style="width:50%;"><?=GetMessage("MLIFESS_IM_PROP_STATUS_OPLY")?> <?=$persName?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "mess_status_".$siteId."_payY_".$persid, "");?>
			<textarea style="width:90%;" name="mess_status_<?=$siteId?>_payY_<?=$persid?>" id="mess_status_<?=$siteId?>_payY_<?=$persid?>"><?=$val?></textarea>
		</td>
	</tr>
	<tr>
		<td style="width:50%;"><?=GetMessage("MLIFESS_IM_PROP_STATUS_OPLN")?>, <?=GetMessage("MLIFESS_IM_PROP_DLYA")?> <?=$persName?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "mess_status_".$siteId."_payN_".$persid, "");?>
			<textarea style="width:90%;" name="mess_status_<?=$siteId?>_payN_<?=$persid?>" id="mess_status_<?=$siteId?>_payN_<?=$persid?>"><?=$val?></textarea>
		</td>
	</tr>
	<tr>
		<td style="width:50%;"><?=GetMessage("MLIFESS_IM_PROP_STATUS_NEW")?>, <?=GetMessage("MLIFESS_IM_PROP_DLYA")?> <?=$persName?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "mess_status_".$siteId."_new_".$persid, "");?>
			<textarea style="width:90%;" name="mess_status_<?=$siteId?>_new_<?=$persid?>" id="mess_status_<?=$siteId?>_new_<?=$persid?>"><?=$val?></textarea>
		</td>
	</tr>
	<tr>
		<td style="width:50%;"><?=GetMessage("MLIFESS_IM_PROP_STATUS_NEWADMIN")?>, <?=GetMessage("MLIFESS_IM_PROP_DLYA")?> <?=$persName?>:</td>
		<td>
			<?$val = COption::GetOptionString("mlife.smsservices", "mess_status_".$siteId."_new2_".$persid, "");?>
			<textarea style="width:90%;" name="mess_status_<?=$siteId?>_new2_<?=$persid?>" id="mess_status_<?=$siteId?>_new2_<?=$persid?>"><?=$val?></textarea>
		</td>
	</tr>
	<?
	}?>
	<?
}
?>
<?
}
?>
	
	<?
$tabControl->BeginNextTab();
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");
$tabControl->Buttons();
?>
	<input <?if ($MODULE_RIGHT<"W") echo "disabled" ?> type="submit" class="adm-btn-green" name="Update" value="<?=GetMessage("MLIFESS_OPT_SEND")?>" />
	<input type="hidden" name="Update" value="Y" />
<?$tabControl->End();
?>
</form>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?> 