<?
if(IsModuleInstalled("mlife.smsservices")) {
	$updater->CopyFiles('/bitrix/modules/mlife.smsservices/install/css', 'css');
	$updater->CopyFiles('/bitrix/modules/mlife.smsservices/install/admin', 'admin');
	global $DB;
	$sql = "alter table `mlife_smsservices_list` add column `EVENT_NAME` varchar(100) NULL DEFAULT NULL";
	$res = $DB->Query($sql);
	$sql = "alter table `mlife_smsservices_list` add column `EVENT` varchar(100) NULL DEFAULT 'DEFAULT'";
	$res = $DB->Query($sql);
	$sql = "
		CREATE TABLE IF NOT EXISTS `mlife_smsservices_eventlist` (
		`ID` int(9) NOT NULL AUTO_INCREMENT,
		`SITE_ID` varchar(10) NOT NULL,
		`SENDER` varchar(50) NULL,
		`EVENT` varchar(50) NOT NULL,
		`NAME` varchar(255) NOT NULL,
		`TEMPLATE` varchar(2500) NULL,
		`PARAMS` varchar(6255) NULL,
		`ACTIVE` varchar(1) NOT NULL DEFAULT 'N',
		 PRIMARY KEY (`ID`)
		);
		";
	$res = $DB->Query($sql);
	
	$eventManager = \Bitrix\Main\EventManager::getInstance();
	$eventManager->registerEventHandlerCompatible('main', 'OnAdminTabControlBegin', 'mlife.smsservices', '\Mlife\Smsservices\Events', 'OnAdminTabControlBegin');
}
?>
