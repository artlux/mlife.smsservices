<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.smsservices
 * @copyright  2015 Zahalski Andrew
 */

namespace Mlife\Smsservices;

class Sender {
	
	protected $transport = null; //главный шлюз
	protected $transport_r = null; //резервный шлюз
	protected $transport_name = null; //название главного шлюза
	protected $transport_r_name = null; //название резервного шлюза
	public $reserve = false; //устанавливает флаг использования резервного шлюза при отправке
	public $translit = null;
	
	//конструктор
	//$defaultTransportInterface (true - установка настроек из параметров модуля, false - использовать свою логику установки);
	//$params - пока не задействован
	function __construct($defaultTransportInterface=true,$params=array()) {
		
		if($defaultTransportInterface){
			
			if($this->translit===null) $this->translit = (\Bitrix\Main\Config\Option::get("mlife.smsservices","translit","N","")=="Y") ? true : false;
			
			//основной шлюз
			$classname = \Bitrix\Main\Config\Option::get("mlife.smsservices","transport","","");
			if($classname){
				if(strpos($classname,'.php')!==false) $classname = str_replace(".php","",$classname);
				$classname = ToUpper(substr($classname,0,1)).substr($classname,1);
				$classname = "\\Mlife\\Smsservices\\Transport\\".$classname;
				$paramsAr = array(
					'login' => \Bitrix\Main\Config\Option::get("mlife.smsservices","login",""),
					'passw' => \Bitrix\Main\Config\Option::get("mlife.smsservices","passw",""),
					'sender' => \Bitrix\Main\Config\Option::get("mlife.smsservices","sender",""),
					'charset' => \Bitrix\Main\Config\Option::get("mlife.smsservices","charset",""),
				);
				$this->setTransport($classname,$paramsAr);
			}
			
			//резервный шлюз
			$classname = \Bitrix\Main\Config\Option::get("mlife.smsservices","transport_r","smsc.php");
			if($classname){
				if(strpos($classname,'.php')!==false) $classname = str_replace(".php","",$classname);
				$classname = ToUpper(substr($classname,0,1)).substr($classname,1);
				$classname = "\\Mlife\\Smsservices\\Transport\\".$classname;
				$this->transport_r = new $classname;
				$paramsAr = array(
					'login' => \Bitrix\Main\Config\Option::get("mlife.smsservices","login_r",""),
					'passw' => \Bitrix\Main\Config\Option::get("mlife.smsservices","passw_r",""),
					'sender' => \Bitrix\Main\Config\Option::get("mlife.smsservices","sender_r",""),
					'charset' => \Bitrix\Main\Config\Option::get("mlife.smsservices","charset_r",""),
				);
				$this->setTransport_r($classname,$paramsAr);
			}
		
		}
	
	}
	
	public function setTransport($transport,$params){
		if(!class_exists($transport)) return false;
		$this->transport = new $transport($params);
		$this->transport_name = ToLower(str_replace("\\Mlife\\Smsservices\\Transport\\","",$transport));
	}
	
	public function setTransport_r($transport,$params){
		if(!class_exists($transport)) return false;
		$this->transport_r = new $transport($params);
		$this->transport_r_name = ToLower(str_replace("\\Mlife\\Smsservices\\Transport\\","",$transport));
	}
	
	/**
	* Метод для проверки номера телефона
	* @param string      $phone    номер телефона для проверки
	* @param boolean     $all      необязательный параметр по умолчанию true (весь мир), false (только снг)
	* @return array                phone - номер без мусора, check - результат проверки(boolean)
	*/
	public function checkPhoneNumber ($phone,$all=true) {
		
		//очистка от лишнего мусора
		$phoneFormat = '+'.preg_replace("/[^0-9A-Za-z]/", "", $phone);
		
		//проверка номера мир
		$pattern_world = "/^\+?([87](?!95[4-79]|99[^2457]|907|94[^0]|336|986)([348]\d|9[0-689]|7[0247])\d{8}|[1246]\d{9,13}|68\d{7}|5[1-46-9]\d{8,12}|55[1-9]\d{9}|55119\d{8}|500[56]\d{4}|5016\d{6}|5068\d{7}|502[45]\d{7}|5037\d{7}|50[457]\d{8}|50855\d{4}|509[34]\d{7}|376\d{6}|855\d{8}|856\d{10}|85[0-4789]\d{8,10}|8[68]\d{10,11}|8[14]\d{10}|82\d{9,10}|852\d{8}|90\d{10}|96(0[79]|17[01]|13)\d{6}|96[23]\d{9}|964\d{10}|96(5[69]|89)\d{7}|96(65|77)\d{8}|92[023]\d{9}|91[1879]\d{9}|9[34]7\d{8}|959\d{7}|989\d{9}|97\d{8,12}|99[^4568]\d{7,11}|994\d{9}|9955\d{8}|996[57]\d{8}|9989\d{8}|380[34569]\d{8}|381\d{9}|385\d{8,9}|375[234]\d{8}|372\d{7,8}|37[0-4]\d{8}|37[6-9]\d{7,11}|30[69]\d{9}|34[67]\d{8}|3[12359]\d{8,12}|36\d{9}|38[1679]\d{8}|382\d{8,9})$/";
		//проверка номера снг
		$pattern_sng = "/^((\+?7|8)(?!95[4-79]|99[^2457]|907|94[^0]|336)([348]\d|9[0-689]|7[07])\d{8}|\+?(99[^456]\d{7,11}|994\d{9}|9955\d{8}|996[57]\d{8}|380[34569]\d{8}|375[234]\d{8}|372\d{7,8}|37[0-4]\d{8}))$/";
		
		if($all) {
			$patt = $pattern_world;
		}
		else {
			$patt = $pattern_sng;
		}
		
		if(!preg_match($patt, $phoneFormat)) {
			return array('phone'=>$phoneFormat,'check'=>false);
		}
		
		return array('phone'=>$phoneFormat,'check'=>true);
	
	}
	
	//отправка смс, пост отправка, добавление записи в историю смс
	public function sendSms($phones, $MEWSS, $time=0, $sender=false, $prim='', $addHistory=true, $update=false, $error=false) {
			
			$arParamsTranslit = array("max_len"=>"1000","change_case"=>"false","replace_space"=>" ","replace_other"=>"","","delete_repeat_replace"=>false,
			"safe_chars"=>'$%&*()_+=-#@!\'"./\\,<>?;:|~`№');
			if($this->translit===true) $MEWSS = \CUtil::translit($MEWSS,"ru",$arParamsTranslit);
			
			if(($time==0 || $time<time()) && !$error) {
				$time = 0;
				if($this->reserve){
					if($this->transport_r===null){
						$send = new \stdClass();
						$send->error = 'Transport reserve not found';
						$send->error_code = '9998';
					}else{
						$send = $this->transport_r->_sendSms($phones, $MEWSS, $time, $sender);
					}
				}else{
					if($this->transport===null){
						$send = new \stdClass();
						$send->error = 'Transport not found';
						$send->error_code = '9998';
					}else{
						$send = $this->transport->_sendSms($phones, $MEWSS, $time, $sender);
					}
				}
				if($send->error_code){
					//TODO надо хорошо потестировать, чтоб не зациклить
					if(!$this->reserve && $this->transport_r!==null){
						$this->reserve = true;
						$res = $this->sendSms($phones, $MEWSS, $time, $sender, $prim, $addHistory, $update, $error);
						$this->reserve = false;
						return $res;
					}else{
						return $this->sendSms($phones, $MEWSS, time(), $sender, $prim.', '.$send->error, $addHistory, $update, array('status'=>12, 'send'=>$send));
					}
				}
			}

			if($addHistory) {
				if(!$sender) $sender = \Bitrix\Main\Config\Option::get("mlife.smsservices","sender".(($this->reserve) ? "_r" : ""),"","");
				if($time==0) {
					$time = time();
					$id = $send->id;
					$status = 2;
				}
				else{
					$id = '-';
					$status = 1;
					if($error) $status = $error['status'];
				}
					$arFields = array(
						'PROVIDER' => str_replace('.php','',\Bitrix\Main\Config\Option::get("mlife.smsservices","transport".(($this->reserve) ? "_r" : ""),"","")),
						'SMSID' => $id,
						'SENDER' => $sender,
						'PHONE' => $phones,
						'TIME' => $time,
						'TIME_ST' => 0,
						'MEWSS' => $MEWSS,
						'PRIM' => $prim,
						'STATUS'=> $status
					);
					\Mlife\Smsservices\ListTable::add($arFields);
				if(!$error) {
					return $send;
				}
				else{
					return $error['send'];
				}
			}
			
			if ($update) {
				
				if($error) {
					$status = $error['status'];
					$sendid = '-';
				}else{
					$status=2;
					$sendid = $send->id;
				}
				$arData = array(
					"STATUS" => $status,
					"SMSID" => $sendid
				);
				\Mlife\Smsservices\ListTable::update(array("ID"=>$update['id']),$arData);
			}
			
			if(!$error) {
				return $send;
			}
			else{
				return $error['send'];
			}
				
	}
	
	public function getBalance(){
		
		$arBalance = array(
			'main' => $this->getBalanceTransport(true),
			'reserve' => $this->getBalanceTransport(false)
		);
		
		return $arBalance;
	
	}
	
	//метод получает отправителей + кеширует ответ
	public function getAllSender($main=true) {
		
		if($this->transport===null && $main) return false;
		if($this->transport_r===null && !$main) return false;
		
		$obCache = \Bitrix\Main\Data\Cache::createInstance();
		if($main){
			$cache_time = \Bitrix\Main\Config\Option::get("mlife.smsservices","cacheotp","86400","");
			$cache_id = 'senders.'.$this->transport_name;
		}else{
			$cache_time = \Bitrix\Main\Config\Option::get("mlife.smsservices","cacheotp_r","86400","");
			$cache_id = 'senders.'.$this->transport_r_name;
		}
		
		if( $obCache->initCache($cache_time,$cache_id,"/mlife/smsservices/admin/") )
		{
			$vars = $obCache->GetVars();
		}
		elseif( $obCache->startDataCache()  )
		{
			if($main){
			$vars = $this->transport->_getAllSender();
			}else{
			$vars = $this->transport_r->_getAllSender();
			}
			if(!$vars->error){
				$obCache->endDataCache($vars);
			}else{
				$obCache->abortDataCache();
			}
		}
		return $vars;
		
	}
	
	//хтмл списка отправителей (options)
	public function getAllSenderOptions($main=true) {
	
		if(\Bitrix\Main\Config\Option::get("mlife.smsservices", "listotp","","")!='Y' && $main) return '';
		if(\Bitrix\Main\Config\Option::get("mlife.smsservices", "listotp_r","","")!='Y' && !$main) return '';
	
		$data = $this->getAllSender($main);
		if($data->error){
			return '';
		}
		else {
		
			$html = '';
			if($main){
				$val = \Bitrix\Main\Config\Option::get("mlife.smsservices", "sender", ".","");
			}else{
				$val = \Bitrix\Main\Config\Option::get("mlife.smsservices", "sender_r", ".","");
			}
			foreach ($data as $value){
				
				$selected = '';
				if($val==$value->sender){
				$selected = ' selected';
				}
				
				$html .= '<option value="'.$value->sender.'"'.$selected.'>'.$value->sender.'</option>';
				
			}
			
			return $html;
			
		}
		
	}
	
	//метод для получения баланса + кеширование ответа
	private function getBalanceTransport($main=true){
		
		if($this->transport===null && $main) return false;
		if($this->transport_r===null && !$main) return false;
		
		$obCache = \Bitrix\Main\Data\Cache::createInstance();
		if($main){
			$cache_time = \Bitrix\Main\Config\Option::get("mlife.smsservices","cachebalance","3600","");
			$cache_id = 'balance.'.$this->transport_name;
		}else{
			$cache_time = \Bitrix\Main\Config\Option::get("mlife.smsservices","cachebalance_r","3600","");
			$cache_id = 'balance.'.$this->transport_r_name;
		}
		
		if( $obCache->initCache($cache_time,$cache_id,"/mlife/smsservices/admin/") )
		{
			$vars = $obCache->GetVars();
		}
		elseif( $obCache->startDataCache()  )
		{
			if($main){
				$vars = $this->transport->_getBalance();
			}else{
				$vars = $this->transport_r->_getBalance();
			}
			
			if(!$vars->error){
				$obCache->endDataCache($vars);
			}
			else{
				$obCache->abortDataCache();
			}
		}
		return $vars;
	}
	
	//получаем список неотправленных смс и отправляем их
	public function getTurnSms() {
		
		$arFilter = array();
		if($this->transport_name!==null) $arFilter["PROVIDER"][] = $this->transport_name;
		if($this->transport_r_name!==null) $arFilter["PROVIDER"][] = $this->transport_r_name;
		$arFilter["STATUS"] = 1;
		$arFilter["<TIME"] = time();
		
		$res = \Mlife\Smsservices\ListTable::getList(
			array(
				'select' => array('ID','SENDER','PHONE','MEWSS','PROVIDER'),
				'filter' => array($arFilter),
				'limit' => \Bitrix\Main\Config\Option::get("mlife.smsservices","limitsms",10,"")
			)
		);
		while ($data = $res->fetch()){
			usleep(100000); //на всякий случай не более 10 запросов в секунду (некоторые шлюзы могут блокировать ip)
			if($data["PROVIDER"]==$this->transport_r_name) {
				$this->reserve = true;
			}else{
				$this->reserve = false;
			}
			$send = $this->sendSms($data['PHONE'], $data['MEWSS'], 0, $data['SENDER'], '', false, array('id'=>$data['ID']));
		}
	
	}
	
	//получаем необновленные статусы и обновляем
	public function getStatusSms() {
		
		$arFilter = array();
		if($this->transport_name!==null) $arFilter["PROVIDER"][] = $this->transport_name;
		if($this->transport_r_name!==null) $arFilter["PROVIDER"][] = $this->transport_r_name;
		$arFilter["STATUS"] = array(0,2,3,6);
		
		$res = \Mlife\Smsservices\ListTable::getList(
			array(
				'select' => array('ID','PHONE','SMSID','PROVIDER'),
				'filter' => array($arFilter),
				'limit' => (\Bitrix\Main\Config\Option::get("mlife.smsservices","limitsms",10,"") * 2)
			)
		);
		
		while ($data = $res->fetch()){
			usleep(100000); //на всякий случай не более 10 запросов в секунду (некоторые шлюзы могут блокировать ip)
			//получаем статус со шлюза
			if($data["PROVIDER"]==$this->transport_r_name) {
				$resp = $this->transport_r->_getStatusSms($data['SMSID'],$data['PHONE']);
			}elseif($data["PROVIDER"]==$this->transport_name){
				$resp = $this->transport->_getStatusSms($data['SMSID'],$data['PHONE']);
			}else{
				$resp = new \stdClass();
				$data->error = 'Service not active. Params error.';
				$data->error_code = '9998';
				return $data;
			}
			//если нет ошибок обновляем в базе
			if(!$resp->error_code) {
				if(!$resp->last_timestamp) $resp->last_timestamp = time();
				\Mlife\Smsservices\ListTable::update(array("ID"=>$data["ID"]),array("STATUS"=>$resp->status,"TIME_ST"=>$resp->last_timestamp));
			}else{
				if(!$resp->last_timestamp) $resp->last_timestamp = time();
				\Mlife\Smsservices\ListTable::update(array("ID"=>$data["ID"]),array("STATUS"=>12,"TIME_ST"=>$resp->last_timestamp));
			}
		}
		
	}
	
}